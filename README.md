# Java Learning of Design Patterns by Example 

List of patterns in this project

1. Singleton pattern - `com.gitlab.pattern.singleton.Driver`
2. Factory pattern = `com.gitlab.pattern.factory.Driver`
