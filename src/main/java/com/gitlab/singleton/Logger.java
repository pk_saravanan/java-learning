package com.gitlab.pattern.singleton;

public class Logger {

    private static Logger logger;

    // prevents us from creating an object
    private Logger() {
    }

    public static Logger getInstance() {
        if (logger == null) {
            logger = new Logger();
        }
        return logger;
    }

}
