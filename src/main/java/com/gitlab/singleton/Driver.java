package com.gitlab.pattern.singleton;

public class Driver {

    public static void main(String[] args) {

        // Not possible because of private constructor
//        Logger logger1 = new Logger();
//        Logger logger2 = new Logger();
//        Logger logger3 = new Logger();

        // correct approach is,
        Logger logger1 = Logger.getInstance();
        Logger logger2 = Logger.getInstance();
        Logger logger3  = Logger.getInstance();

        System.out.println(logger1.hashCode());
        System.out.println(logger2.hashCode());
        System.out.println(logger3.hashCode());
    }
}
