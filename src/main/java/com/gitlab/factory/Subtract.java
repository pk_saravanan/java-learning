package com.gitlab.pattern.factory;

public class Subtract implements Calculate {

    @Override
    public void calculate(double number1, double number2) {
        System.out.println(number1 - number2);
    }
}
