package com.gitlab.pattern.factory;

public class Divide implements Calculate{

    @Override
    public void calculate(double number1, double number2) {
        System.out.println("result is => " + (number1 / number2));
    }
}
