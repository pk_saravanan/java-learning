package com.gitlab.pattern.factory;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter first Number: ");
        double number1 = scanner.nextDouble();

        System.out.println("Enter second Number: ");
        double number2 = scanner.nextDouble();

//        Traditional Approach
//        Divide obj = new Divide();
//        obj.calculate(number1, number2);

        scanner.nextLine();

        // Correct approach
        CalculateFactory calculateFactory = new CalculateFactory();
        Calculate cal = calculateFactory.getCalculation(scanner.nextLine());

        cal.calculate(number1, number2);

    }
}
