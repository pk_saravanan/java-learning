package com.gitlab.pattern.factory;

public interface Calculate {

    public void calculate(double number1, double number2);
}
